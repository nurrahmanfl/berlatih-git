/*1. Membuat Database*/

CREATE DATABASE myshop;



/*2. Membuat Table di Dalam Database*/

CREATE TABLE users(
	id INT PRIMARY KEY AUTO_INCREMENT,
	name VARCHAR(255),
	email VARCHAR(255),
    	password VARCHAR(255)
);

CREATE TABLE categories(
	id INT PRIMARY KEY AUTO_INCREMENT,
	name VARCHAR(255)
);

CREATE TABLE items(
	id INT PRIMARY KEY AUTO_INCREMENT,
    	name VARCHAR(255),
    	description VARCHAR(255),
    	price INT,
    	stock INT,
    	category_id INT,
    	FOREIGN KEY (category_id) REFERENCES categories(id)
);




/*3. Memasukkan Data pada Table*/

INSERT INTO users(name, email,password) VALUES("John Doe", "john@doe.com", "john123");
INSERT INTO users(name, email,password) VALUES("Jane Doe","jane@doe.com","jenita123");

INSERT INTO categories(name) VALUES("gadget");
INSERT INTO categories(name) VALUES("cloth");
INSERT INTO categories(name) VALUES("men");
INSERT INTO categories(name) VALUES("woman");
INSERT INTO categories(name) VALUES("branded");

INSERT INTO items(name, description, price, stock, category_id) VALUES("Sumsang b50","hape keren dari merek sumsang",4000000,100,1);
INSERT INTO items(name, description, price, stock, category_id) VALUES("Uniklooh","baju keren dari brand ternama",500000,50,2);
INSERT INTO items(name, description, price, stock, category_id) VALUES("IMHO Watch","jam tangan anak yang jujur banget",2000000,10,1);



/*4. Mengambil Data dari Database*/

/*a. Mengambil data users*/

SELECT id,name,email FROM users;

/*b. Mengambil data items*/

SELECT * FROM items WHERE price>=1000000;
SELECT * FROM items WHERE name LIKE "%sang%";

/*c. Menampilkan data items join dengan kategori*/

SELECT i.name, i.description, i.price, i.stock, i.category_id, c.name
FROM items i JOIN categories c
ON i.category_id=c.id;



/*5. Mengubah Data dari Database*/

UPDATE items SET price=2500000 WHERE name="Sumsang b50";