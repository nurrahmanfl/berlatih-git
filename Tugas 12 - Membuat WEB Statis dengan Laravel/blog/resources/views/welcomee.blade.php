<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8" />
    <meta name="description" content="sanbercode HTML" />
    <meta name="author" content="Mohammad Nurrahman Bahtiar" />
    <meta name="keyword" content="HTML,sanbercode,sekolahcoding" />

    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />

    <title>SanberBook</title>
  </head>
  <body>
    <h1>SELAMAT DATANG {{$nama_depan}} {{$nama_belakang}} !</h1>
    <h2>
      Terima kasih telah bergabung di Sanberbook. Social Media kita bersama!
    </h2>

    <h3><a href="/">Back</h3>
  </body>
</html>
