<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8" />
    <meta name="description" content="sanbercode HTML" />
    <meta name="author" content="Mohammad Nurrahman Bahtiar" />
    <meta name="keyword" content="HTML,sanbercode,sekolahcoding" />

    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />

    <title>SanberBook</title>
  </head>
  <body>
    <h1>Buat Account Baru!</h1>
    <h2>Sign Up Form</h2>
    <form action="/kirim" method="post">
      @csrf
      <label>First Name:</label><br />
      <input type="text" name="firstName" /> <br />
      <label>Last Name:</label><br />
      <input type="text" name="lastName" /> <br />

      <label>Gender:</label><br />
      <input type="radio" name="gender" value="male" />Male <br />
      <input type="radio" name="gender" value="female" />Female <br />
      <input type="radio" name="gender" value="other" />Other <br />

      <label>Nationality:</label><br />
      <select name="nationaly">
        <option value="Indonesian">Indonesian</option>
        <option value="Malaysian">Malaysian</option>
        <option value="Other">Other</option></select
      ><br />

      <label>Language Spoken:</label><br />
      <input name="language" type="checkbox" value="Bahasa Indonesia" />Bahasa Indonesia<br />
      <input name="language" type="checkbox" value="English" />English<br />
      <input name="language" type="checkbox" value="Other" />Other<br />

      <label>Bio</label><br />
      <textarea name="bio" cols="50" rows="4"></textarea>
      <br />
      <input type="submit" value="kirim" />
    </form>
  </body>
</html>
