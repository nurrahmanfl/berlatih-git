<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class CastController extends Controller
{
    //
    public function create(){
        return view('cast.create');
    }

    public function store_db(Request $request){
        // uji coba datanya masuk apa tidak
        // dd($request->all());
        $request->validate([
                'nama' => 'required',
                'umur' => 'required',
                'bio' => 'required|min:5',
            ],
            [
                'nama.required' => 'nama tidak boleh kosong',
                'umur.required' => 'umur tidak boleh kosong',
                'bio.required' => 'bio tidak boleh kosong',
                'bio.min' => 'bio min 6 karakter',
                
            ]
        );

        DB::table('cast')->insert(
            [
                'nama' => $request['nama'], 
                'umur' => $request['umur'],
                'bio' => $request['bio']
            ]
        );

        return redirect('/cast');
    
    }

    //////////////////////////////////////////////////////////////////////////////
    
    
    public function index(){
        $cast = DB::table('cast')->get();

        // uji coba datanya masuk apa tidak
        // dd($cast);
        
        return view('cast.index', compact('cast'));
    }

    public function show($id){
        $cast = DB::table('cast')->where('id', $id)->first();
        
        return view('cast.detail',compact('cast'));
    }

    
    //////////////////////////////////////////////////////////////////////////////

    public function edit($id){
        $cast = DB::table('cast')->where('id', $id)->first();
        
        return view('cast.update',compact('cast'));
    }

    public function update(Request $request, $id){
        $request->validate(
            [
                'nama' => 'required',
                'umur' => 'required',
                'bio' => 'required|min:5',
            ],
            [
                'nama.required' => 'nama tidak boleh kosong',
                'umur.required' => 'umur tidak boleh kosong',
                'bio.required' => 'bio tidak boleh kosong',
                'bio.min' => 'bio min 6 karakter',
                
            ]
        );

        DB::table('cast')
              ->where('id', $id)
              ->update(
                [
                    'nama' => $request['nama'],
                    'umur' => $request['umur'],
                    'bio' => $request['bio'],
                ]
            );
        return redirect('/cast');
    }

    
    //////////////////////////////////////////////////////////////////////////////

    public function destroy($id){
        DB::table('cast')->where('id', $id)->delete();
        
        return redirect('/cast');
    }
}
