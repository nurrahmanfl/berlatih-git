<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/','HomeController@home');
Route::get('/register','AuthController@register');
Route::post('/kirim','AuthController@kirim');

Route::get('/table',function(){
    return view('pages.table');
});

Route::get('/data-tables',function(){
    return view('pages.data-tables');
});



//########## CRUD


//#CREATE ke cast
Route::get('/cast/create','CastController@create');
//untuk kirim data ke DB cast
Route::post('/cast','CastController@store_db');


//#READ data cast

//tampil semua data kategori
Route::get('/cast','CastController@index');
//detail cast berdasarkan id
Route::get('/cast/{id}','CastController@show');


//#UPDATE data cast

//masuk form cast berdasarkan id
Route::get('/cast/{id}/edit','CastController@edit');
//untuk update data inputan berdasarkan id
Route::put('/cast/{id}','CastController@update');

//#DELETE data cast

//delete data berdasarkan parameter id
Route::delete('/cast/{id}','CastController@destroy');


// Route::get('/table', function(){
//     return view('layout.master');
// });
    
// Route::get('home/{nama}',function($nama){
//     return "SELAMAT DATANG $nama";
// });
