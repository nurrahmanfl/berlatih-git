@extends('layout.master')
    
@section('judul')
  Halaman Detail Cast
@endsection

@section('isi')
<h1 class="text-primary">{{$cast->nama}}</h1>
<h2 class="text-secondary">{{$cast->umur}}</h2>
<p>{{$cast->bio}}</p>

<a href="/cast" class="btn btn-secondary btn-sm">Kembali</a>
@endsection
   
