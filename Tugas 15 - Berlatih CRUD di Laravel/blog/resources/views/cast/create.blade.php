@extends('layout.master')
    
@section('judul')
  Halaman Tambah Cast
@endsection

@section('isi')
<form action="/cast" method="post">
    @csrf
    <div class="card-body">
        <div class="form-group">
        <label>Nama Cast</label>
        @error('nama')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror
        <input name="nama" value="{{old('nama')}}" type="text" class="form-control">
        </div>
        <div class="form-group">
        <label>Umur</label>
        @error('umur')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror
        <input name="umur" value="{{old('umur')}}" type="text" class="form-control">
        </div>
        <div class="form-group">
        <label>Bio</label>
        @error('bio')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror
        <textarea name="bio" value="{{old('bio')}}" class="form-control" rows="3"></textarea>
        </div>

    </div>
    <!-- /.card-body -->

    <div class="card-footer">
        <button type="submit" class="btn btn-primary">Submit</button>
    </div>
</form>
@endsection
   
