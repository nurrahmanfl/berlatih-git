@extends('layout.master')
    
@section('judul')
  Halaman List Cast
@endsection

@section('isi')
<table class="table table-bordered">

<a href="/cast/create" class="btn btn-primary btn-sm mb-4">Tambah</a>

  <thead>                  
    <tr>
      <th style="width: 10px">No</th>
      <th>Nama</th>
      <th style="width: 20%">Aksi</th>
    </tr>
  </thead>
  <tbody>
    @forelse ($cast as $key => $item)
        <tr>
            <td>{{$key+1}}</td>
            <td>{{$item->nama}}</td>
            <td>
                <form action="/cast/{{$item->id}}" method="post">
                    @csrf
                    @method('delete')
                    <a href="/cast/{{$item->id}}" class="btn btn-info btn-sm">Detail</a>
                    <a href="/cast/{{$item->id}}/edit" class="btn btn-warning btn-sm">Edit</a>
                    <input type="submit" value="Delete" class="btn btn-danger btn-sm">
                </form>
            </td>
        </tr>
    @empty
        <tr>
            <td>Tidak ada data</td>
        </tr>
    @endforelse
  </tbody>
</table>
@endsection
   
