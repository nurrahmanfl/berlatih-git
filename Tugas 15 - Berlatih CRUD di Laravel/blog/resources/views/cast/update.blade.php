@extends('layout.master')
    
@section('judul')
  Halaman Update Cast
@endsection

@section('isi')
<form action="/cast/{{$cast->id}}" method="POST">
    @csrf
    @method('put')
    <div class="card-body">
        <div class="form-group">
        <label>Nama Cast</label>
        @error('nama')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror
        <input name="nama" value="{{old('nama',$cast->nama)}}" type="text" class="form-control">
        </div>
        <div class="form-group">
        <label>Umur</label>
        @error('umur')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror
        <input name="umur" value="{{old('umur',$cast->umur)}}" type="text" class="form-control">
        </div>
        <div class="form-group">
        <label>Bio</label>
        @error('bio')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror
        <textarea name="bio" class="form-control" rows="3">{{old('bio',$cast->bio)}}</textarea>
        </div>

    </div>
    <!-- /.card-body -->

    <div class="card-footer">
        <button type="submit" class="btn btn-primary">Submit</button>
    </div>
</form>
@endsection
   
