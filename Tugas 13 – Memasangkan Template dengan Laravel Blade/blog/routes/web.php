<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/','HomeController@home');

Route::get('/register','AuthController@register');

Route::post('/kirim','AuthController@kirim');

Route::get('/table',function(){
    return view('pages.table');
});

Route::get('/data-tables',function(){
    return view('pages.data-tables');
});

// Route::get('/table', function(){
//     return view('layout.master');
// });
    
// Route::get('home/{nama}',function($nama){
//     return "SELAMAT DATANG $nama";
// });
